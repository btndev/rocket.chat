FROM node:8.9-slim

ADD ./app /app

RUN set -x \
  && cd /app/build/bundle/programs/server/ \
  && npm install

WORKDIR /app/build/bundle

ENV PORT=3000 \
    ROOT_URL=https://chat.worldfixer.com

EXPOSE 3000

CMD ["node", "main.js"]
